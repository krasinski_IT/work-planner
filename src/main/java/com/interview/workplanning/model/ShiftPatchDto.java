package com.interview.workplanning.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class ShiftPatchDto {

    @NotNull
    private LocalDateTime startDate;
    @NotNull
    private LocalDateTime endDate;
}
