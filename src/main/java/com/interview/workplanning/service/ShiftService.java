package com.interview.workplanning.service;

import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.producer.ShiftsEvents;
import com.interview.workplanning.repository.ShiftRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@RequiredArgsConstructor
public class ShiftService {

    private final ShiftRepository shiftRepository;
    private final ShiftValidator shiftValidator;
    private final ShiftsEvents shiftsEvents;

    public List<Shift> getShiftsForWorkerId(String workerId, LocalDateTime startDate, LocalDateTime endDate) {

        if (startDate != null && endDate != null) {
            return shiftRepository.findAllByWorkerIdAndEndDateGreaterThanAndStartDateLessThan(workerId, startDate, endDate);
        } else if (startDate != null) {
            return shiftRepository.findAllByWorkerIdAndEndDateGreaterThan(workerId, startDate);
        } else if (endDate != null) {
            return shiftRepository.findAllByWorkerIdAndStartDateLessThan(workerId, endDate);
        } else {
            return shiftRepository.findAllByWorkerId(workerId);
        }
    }

    @Transactional
    public Shift createShift(Shift shift) {

        shiftValidator.validateShift(shift, null);
        shiftsEvents.sendCreatedShiftEvent(shift);

        return shiftRepository.save(shift);
    }

    public void deleteShift(Long id) {
        shiftRepository.deleteById(id);
        shiftsEvents.sendDeletedShiftEvent(id);
    }

    @Transactional
    public void patchShift(Long id, Shift shift) {

        Shift actualShift = shiftRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, NOT_FOUND.getReasonPhrase()));

        shiftValidator.validateShift(shift, actualShift.getId());

        actualShift.setStartDate(shift.getStartDate());
        actualShift.setEndDate(shift.getEndDate());

        shiftsEvents.sendPatchedShiftEvent(shift);
    }
}
