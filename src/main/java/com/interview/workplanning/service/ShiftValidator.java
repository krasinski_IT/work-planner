package com.interview.workplanning.service;

import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.repository.ShiftRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShiftValidator {

    @Value("${shifts.validations.min-hours}")
    private Integer minHours;
    @Value("${shifts.validations.max-hours}")
    private Integer maxHours;
    @Value("${shifts.validations.rest-hours}")
    private Integer restHours;

    private final ShiftRepository shiftRepository;

    public static final String MIN_ERROR_MSG = "Time between start date and end date has to be more or equal %s hours";
    public static final String MAX_ERROR_MSG = "Time between start date and end date has to be less or equal to %s hours";
    public static final String REST_ERROR_MSG = "Time between existing shifts has to be more or equal to %s hours";

    public void validateShift(Shift shift) {
        validateShift(shift, null);
    }

    public void validateShift(Shift shift, Long idToIgnore) {

        String workerId = shift.getWorkerId();
        LocalDateTime startDate = shift.getStartDate();
        LocalDateTime endDate = shift.getEndDate();

        Duration durationBetween = Duration.between(startDate, endDate);

        if (durationBetween.toSeconds() < minHours * 3600) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format(MIN_ERROR_MSG, minHours));
        }

        if (durationBetween.toSeconds() > maxHours * 3600) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format(MAX_ERROR_MSG, maxHours));
        }

        List<Shift> shifts;

        if (idToIgnore != null) {
            shifts = shiftRepository.findAllByWorkerIdAndEndDateGreaterThanAndStartDateLessThan(workerId, startDate.minusHours(restHours), endDate.plusHours(restHours))
                    .stream()
                    .filter(s -> s.getId() != idToIgnore)
                    .collect(Collectors.toList());
        } else {
            shifts = shiftRepository.findAllByWorkerIdAndEndDateGreaterThanAndStartDateLessThan(workerId, startDate.minusHours(restHours), endDate.plusHours(restHours));
        }

        if (!shifts.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format(REST_ERROR_MSG, restHours));
        }
    }
}
