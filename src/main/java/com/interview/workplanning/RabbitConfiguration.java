package com.interview.workplanning;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class RabbitConfiguration {

    public static final String NEW_SHIFT_QUEUE = "new-shift-queue";
    public static final String NEW_SHIFT_EXCHANGE = "new-shift-exchange";

    public static final String CREATED_SHIFT_EXCHANGE = "created-shift-exchange";
    public static final String DELETED_SHIFT_EXCHANGE = "deleted-shift-exchange";
    public static final String PATCHED_SHIFT_EXCHANGE = "patched-shift-exchange";

    @Bean
    Queue newShiftQueue() {
        return QueueBuilder
                .durable(NEW_SHIFT_QUEUE)
                .build();
    }

    @Bean
    FanoutExchange newShiftExchange() {
        return new FanoutExchange(NEW_SHIFT_EXCHANGE);
    }

    @Bean
    Binding parkingLotBinding() {
        return BindingBuilder
                .bind(newShiftQueue())
                .to(newShiftExchange());
    }

    @Bean
    FanoutExchange createdShiftExchange() {
        return new FanoutExchange(CREATED_SHIFT_EXCHANGE);
    }

    @Bean
    FanoutExchange deletedShiftExchange() {
        return new FanoutExchange(DELETED_SHIFT_EXCHANGE);
    }

    @Bean
    FanoutExchange patchedShiftExchange() {
        return new FanoutExchange(PATCHED_SHIFT_EXCHANGE);
    }

}
