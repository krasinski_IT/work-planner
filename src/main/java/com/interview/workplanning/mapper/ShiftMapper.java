package com.interview.workplanning.mapper;

import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.model.ShiftDto;
import com.interview.workplanning.model.ShiftPatchDto;
import org.springframework.stereotype.Service;

@Service
public class ShiftMapper {

    public Shift toShift(ShiftDto shiftDto) {
        return Shift.builder()
                .workerId(shiftDto.getWorkerId())
                .startDate(shiftDto.getStartDate())
                .endDate(shiftDto.getEndDate())
                .build();
    }

    public Shift toShift(ShiftPatchDto shiftDto) {
        return Shift.builder()
                .startDate(shiftDto.getStartDate())
                .endDate(shiftDto.getEndDate())
                .build();
    }
}
