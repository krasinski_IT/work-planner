package com.interview.workplanning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkPlanningApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkPlanningApplication.class, args);
    }
}
