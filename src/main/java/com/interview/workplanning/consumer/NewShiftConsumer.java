package com.interview.workplanning.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.workplanning.mapper.ShiftMapper;
import com.interview.workplanning.model.ShiftDto;
import com.interview.workplanning.service.ShiftService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.interview.workplanning.RabbitConfiguration.NEW_SHIFT_QUEUE;

@Service
@RequiredArgsConstructor
@Slf4j
public class NewShiftConsumer {

    private final ShiftService shiftService;
    private final ShiftMapper shiftMapper;
    private final ObjectMapper objectMapper;

    @RabbitListener(queues = NEW_SHIFT_QUEUE)
    public void newShiftMessage(Message message) throws IOException {

        ShiftDto shiftDto = objectMapper.readValue(message.getBody(), ShiftDto.class);
        shiftService.createShift(shiftMapper.toShift(shiftDto));
    }
}
