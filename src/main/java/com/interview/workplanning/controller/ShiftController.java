package com.interview.workplanning.controller;

import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.mapper.ShiftMapper;
import com.interview.workplanning.model.ErrorMsg;
import com.interview.workplanning.model.ShiftDto;
import com.interview.workplanning.model.ShiftPatchDto;
import com.interview.workplanning.service.ShiftService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/v1/api/shifts")
@RequiredArgsConstructor
@Api(tags = {"Shifts"})
@SwaggerDefinition
public class ShiftController {

    private final ShiftService shiftService;
    private final ShiftMapper shiftMapper;

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful read"),
            @ApiResponse(code = 400, message = "Bad request", response = ErrorMsg.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMsg.class)
    })
    @ApiOperation(value = "Get all shifts for worker id and optional params")
    @GetMapping("/{workerId}")
    public List<Shift> getShiftsForWorkerIdAndDates(@PathVariable String workerId,
                                                    @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
                                                    @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate) {
        return shiftService.getShiftsForWorkerId(workerId, startDate, endDate);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad request", response = ErrorMsg.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMsg.class)
    })
    @ApiOperation(value = "Create new shift")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Shift createShift(@RequestBody @Valid ShiftDto shiftDto) {
        return shiftService.createShift(shiftMapper.toShift(shiftDto));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No content"),
            @ApiResponse(code = 400, message = "Bad request", response = ErrorMsg.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMsg.class)
    })
    @ApiOperation(value = "Delete shift")
    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteShift(@PathVariable Long id) {
        shiftService.deleteShift(id);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No content"),
            @ApiResponse(code = 400, message = "Bad request", response = ErrorMsg.class),
            @ApiResponse(code = 404, message = "Not found", response = ErrorMsg.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMsg.class)
    })
    @ApiOperation(value = "Patch shift")
    @PatchMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void patchShift(@PathVariable Long id, @RequestBody @Valid ShiftPatchDto shiftPatchDto) {
        shiftService.patchShift(id, shiftMapper.toShift(shiftPatchDto));
    }
}
