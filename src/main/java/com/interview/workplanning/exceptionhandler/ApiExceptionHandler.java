package com.interview.workplanning.exceptionhandler;

import com.interview.workplanning.model.ErrorMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleThrowable(Exception e, WebRequest webRequest) {

        log.error("Work planner API Exception", e);

        return handleExceptionInternal(new Exception(e),
                ErrorMsg.builder()
                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .error(e.getMessage())
                        .build(),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, webRequest);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Object> handleResponseStatusException(ResponseStatusException e, WebRequest webRequest) {
        return getResponseEntity(e, webRequest);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleValidationException(ValidationException e) {
        throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
    }

    private ResponseEntity<Object> getResponseEntity(ResponseStatusException e, WebRequest webRequest) {

        HttpStatus status = e.getStatus();
        String reason = e.getReason();

        log.error("Work planner API Exception, status: {}, reason: {}", status.value(), reason, e);

        return handleExceptionInternal(new Exception(e),
                ErrorMsg.builder()
                        .code(status.value())
                        .error(reason)
                        .build(),
                new HttpHeaders(), status, webRequest);
    }
}
