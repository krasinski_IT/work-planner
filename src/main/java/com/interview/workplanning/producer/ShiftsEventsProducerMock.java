package com.interview.workplanning.producer;

import com.interview.workplanning.domain.Shift;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Profile("!rabbit-docker")
public class ShiftsEventsProducerMock implements ShiftsEvents {

    public void sendCreatedShiftEvent(Shift shift) {
        log.info("Mocked sendCreatedShiftEvent: {}", shift);
    }

    public void sendDeletedShiftEvent(Long id) {
        log.info("Mocked sendDeletedShiftEvent: {}", id);
    }

    public void sendPatchedShiftEvent(Shift shift) {
        log.info("Mocked sendPatchedShiftEvent: {}", shift);
    }
}
