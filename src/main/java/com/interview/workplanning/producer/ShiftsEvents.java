package com.interview.workplanning.producer;

import com.interview.workplanning.domain.Shift;

public interface ShiftsEvents {

    void sendCreatedShiftEvent(Shift shift);

    void sendDeletedShiftEvent(Long id);

    void sendPatchedShiftEvent(Shift shift);
}
