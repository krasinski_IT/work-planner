package com.interview.workplanning.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.workplanning.domain.Shift;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static com.interview.workplanning.RabbitConfiguration.*;

@Service
@RequiredArgsConstructor
@Slf4j
@Profile("rabbit-docker")
public class ShiftsEventsProducer implements ShiftsEvents {

    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @SneakyThrows
    public void sendCreatedShiftEvent(Shift shift) {
        rabbitTemplate.convertAndSend(CREATED_SHIFT_EXCHANGE, "", objectMapper.writeValueAsString(shift));
    }

    @SneakyThrows
    public void sendDeletedShiftEvent(Long id) {
        rabbitTemplate.convertAndSend(DELETED_SHIFT_EXCHANGE, "", objectMapper.writeValueAsString(id));
    }

    @SneakyThrows
    public void sendPatchedShiftEvent(Shift shift) {
        rabbitTemplate.convertAndSend(PATCHED_SHIFT_EXCHANGE, "", objectMapper.writeValueAsString(shift));
    }
}
