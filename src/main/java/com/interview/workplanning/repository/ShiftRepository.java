package com.interview.workplanning.repository;

import com.interview.workplanning.domain.Shift;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ShiftRepository extends JpaRepository<Shift, Long> {

    List<Shift> findAllByWorkerId(String workerId);

    List<Shift> findAllByWorkerIdAndEndDateGreaterThanAndStartDateLessThan(String workerId, LocalDateTime startDate, LocalDateTime endDate);

    List<Shift> findAllByWorkerIdAndEndDateGreaterThan(String workerId, LocalDateTime startDate);

    List<Shift> findAllByWorkerIdAndStartDateLessThan(String workerId, LocalDateTime endDate);
}
