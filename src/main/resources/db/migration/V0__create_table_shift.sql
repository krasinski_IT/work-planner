CREATE TABLE shift
(
    id         BIGINT IDENTITY (1,1),
    worker_id  VARCHAR(36),
    start_date DATETIME,
    end_date   DATETIME,
    CONSTRAINT PK_SHIFT_ID PRIMARY KEY (id)
);

CREATE INDEX IX_SHIFT_WORKER_ID on shift (worker_id);
