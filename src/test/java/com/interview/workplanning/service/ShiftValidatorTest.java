package com.interview.workplanning.service;

import com.interview.workplanning.WorkPlannerTestBase;
import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.repository.ShiftRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

import static com.interview.workplanning.service.ShiftValidator.*;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ShiftValidatorTest extends WorkPlannerTestBase {

    @Autowired
    private ShiftRepository shiftRepository;
    @Autowired
    private ShiftValidator shiftValidator;

    @BeforeEach
    public void setUp() {
        shiftRepository.deleteAll();
    }

    @Test
    void shouldThrowExceptionWhenShiftIsShorterThanMinValue() {

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.plusHours(minHours).minusSeconds(1))
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(shift);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(MIN_ERROR_MSG, minHours)));
    }

    @Test
    void shouldValidWhenShiftIsEqualsMinValue() {

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.plusHours(minHours))
                .build();

        shiftValidator.validateShift(shift);
    }

    @Test
    void shouldThrowExceptionWhenShiftIsLongerThanMaxValue() {

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours).plusSeconds(1))
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(shift);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(MAX_ERROR_MSG, maxHours)));
    }

    @Test
    void shouldValidWhenShiftIsEqualsMaxValue() {

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours))
                .build();

        shiftValidator.validateShift(shift);
    }

    @Test
    void shouldThrowExceptionWhenEndDateIsLowerThanStartDate() {

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.minusHours(maxHours))
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(shift);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(MIN_ERROR_MSG, minHours)));
    }

    @Test
    void shouldThrowExceptionWhenHaveShiftBeforeWithToShortRestBetween() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2021, 3, 25, 18, 0, 0);

        Shift existingShift1 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate.minusHours(restHours).plusSeconds(1))
                .endDate(startDate.minusHours(restHours).plusSeconds(1).plusHours(minHours))
                .build();

        shiftValidator.validateShift(existingShift1);
        shiftRepository.save(existingShift1);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(shift);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(REST_ERROR_MSG, restHours)));
    }

    @Test
    void shouldThrowExceptionWhenHaveShiftAfterWithToShortRestBetween() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2021, 3, 25, 18, 0, 0);

        Shift existingShift1 = Shift.builder()
                .workerId(workerId)
                .startDate(endDate.plusHours(restHours).minusSeconds(1))
                .endDate(endDate.plusHours(restHours).minusSeconds(1).plusHours(minHours))
                .build();

        shiftValidator.validateShift(existingShift1);
        shiftRepository.save(existingShift1);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(shift);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(REST_ERROR_MSG, restHours)));
    }

    @Test
    void shouldThrowExceptionWhenHaveShiftInTheSameTime() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2021, 3, 25, 18, 0, 0);

        Shift existingShift1 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        shiftValidator.validateShift(existingShift1);
        shiftRepository.save(existingShift1);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(shift);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(REST_ERROR_MSG, restHours)));
    }

    @Test
    void shouldValidWhenShiftBeforeHasGoodRest() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2021, 3, 25, 18, 0, 0);

        Shift existingShift1 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate.minusHours(restHours).minusHours(minHours))
                .endDate(startDate.minusHours(restHours))
                .build();

        shiftValidator.validateShift(existingShift1);
        shiftRepository.save(existingShift1);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        shiftValidator.validateShift(shift);
    }

    @Test
    void shouldValidWhenShiftAfterHasGoodRest() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2021, 3, 25, 18, 0, 0);

        Shift existingShift1 = Shift.builder()
                .workerId(workerId)
                .startDate(endDate.plusHours(restHours))
                .endDate(endDate.plusHours(restHours).plusHours(minHours))
                .build();

        shiftValidator.validateShift(existingShift1);
        shiftRepository.save(existingShift1);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        shiftValidator.validateShift(shift);
    }

    @Test
    void shouldThrowExceptionWhenUpdatedShiftIsShorterThanMinValue() {

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        final Shift shift = shiftRepository.save(Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours))
                .build());

        Shift updatedShift = Shift.builder()
                .workerId("worker-id-1")
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours).plusSeconds(1))
                .build();

        Exception exception = assertThrows(ResponseStatusException.class, () -> {
            shiftValidator.validateShift(updatedShift, shift.getId());
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(format(MAX_ERROR_MSG, maxHours)));
    }
}
