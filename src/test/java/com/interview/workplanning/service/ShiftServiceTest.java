package com.interview.workplanning.service;

import com.interview.workplanning.WorkPlannerTestBase;
import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.repository.ShiftRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShiftServiceTest extends WorkPlannerTestBase {

    @Autowired
    private ShiftService shiftService;
    @Autowired
    private ShiftRepository shiftRepository;

    @BeforeEach
    public void setUp() {
        shiftRepository.deleteAll();
    }

    @Test
    void shouldGetShiftsForWorkerIdAndStartDateAndEndDate() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        prepareTestData(workerId, startDate);

        List<Shift> shifts1 = shiftService.getShiftsForWorkerId(workerId, startDate.minusYears(100), startDate);
        assertEquals(0, shifts1.size());

        List<Shift> shifts2 = shiftService.getShiftsForWorkerId(workerId, startDate.minusYears(100), startDate.plusSeconds(1));
        assertEquals(1, shifts2.size());

        List<Shift> shifts3 = shiftService.getShiftsForWorkerId(workerId, startDate.plusHours(8), startDate.plusDays(1));
        assertEquals(0, shifts3.size());

        List<Shift> shifts4 = shiftService.getShiftsForWorkerId(workerId, startDate.plusHours(8).minusSeconds(1), startDate.plusDays(1));
        assertEquals(1, shifts4.size());

        List<Shift> shifts5 = shiftService.getShiftsForWorkerId(workerId, startDate.plusHours(8), startDate.plusDays(1).plusSeconds(1));
        assertEquals(1, shifts5.size());

        List<Shift> shifts6 = shiftService.getShiftsForWorkerId(workerId, startDate.plusHours(8).minusSeconds(1), startDate.plusDays(1).plusSeconds(1).plusSeconds(1));
        assertEquals(2, shifts6.size());
    }

    @Test
    void shouldGetShiftsForWorkerIdAndStartDate() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        prepareTestData(workerId, startDate);

        List<Shift> shifts1 = shiftService.getShiftsForWorkerId(workerId, startDate.minusDays(1), null);
        assertEquals(4, shifts1.size());

        List<Shift> shifts2 = shiftService.getShiftsForWorkerId(workerId, startDate.plusHours(8), null);
        assertEquals(3, shifts2.size());

        List<Shift> shifts3 = shiftService.getShiftsForWorkerId(workerId, startDate.minusYears(100), null);
        assertEquals(4, shifts3.size());

        List<Shift> shifts4 = shiftService.getShiftsForWorkerId(workerId, startDate.plusDays(2).plusHours(8).plusHours(4).minusSeconds(1), null);
        assertEquals(2, shifts4.size());

        List<Shift> shifts5 = shiftService.getShiftsForWorkerId(workerId, startDate.plusYears(100), null);
        assertEquals(0, shifts5.size());
    }

    @Test
    void shouldGetShiftsForWorkerIdAndEndDate() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        prepareTestData(workerId, startDate);

        List<Shift> shifts1 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(2021, 3, 25, 14, 0, 0));
        assertEquals(0, shifts1.size());

        List<Shift> shifts2 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(2021, 3, 25, 14, 0, 1));
        assertEquals(1, shifts2.size());

        List<Shift> shifts3 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(2021, 3, 26, 14, 0, 0));
        assertEquals(1, shifts3.size());

        List<Shift> shifts4 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(2021, 3, 27, 22, 0, 1));
        assertEquals(3, shifts4.size());

        List<Shift> shifts5 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(2021, 3, 28, 10, 0, 0));
        assertEquals(3, shifts5.size());

        List<Shift> shifts6 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(2021, 3, 28, 18, 0, 1));
        assertEquals(4, shifts6.size());

        List<Shift> shifts7 = shiftService.getShiftsForWorkerId(workerId, null, LocalDateTime.of(1990, 3, 28, 12, 0, 1));
        assertEquals(0, shifts7.size());
    }

    @Test
    void shouldCreateShift() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        shiftService.createShift(Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(8))
                .build());

        assertEquals(1, shiftRepository.findAll().size());
    }

    @Test
    void shouldDeleteShift() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(8))
                .build();
        shiftService.createShift(shift);

        assertEquals(1, shiftRepository.findAll().size());

        shiftService.deleteShift(shift.getId());

        assertEquals(0, shiftRepository.findAll().size());
    }

    @Test
    void shouldPatchShift() {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);

        Shift shift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(8))
                .build();
        shiftService.createShift(shift);

        assertEquals(1, shiftRepository.findAll().size());

        Shift newShift = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(8))
                .build();

        shiftService.patchShift(shift.getId(), newShift);

        Optional<Shift> updatedShift = shiftRepository.findById(shift.getId());

        assertEquals(newShift.getStartDate(), updatedShift.get().getStartDate());
        assertEquals(newShift.getEndDate(), updatedShift.get().getEndDate());

        assertEquals(1, shiftRepository.findAll().size());
    }

    private void prepareTestData(String workerId, LocalDateTime startDate) {

        Shift shift1 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(8))
                .build();

        Shift shift2 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate.plusDays(1))
                .endDate(startDate.plusDays(1).plusHours(8))
                .build();

        Shift shift3 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate.plusDays(2).plusHours(8))
                .endDate(startDate.plusDays(2).plusHours(8).plusHours(4))
                .build();

        Shift shift4 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate.plusDays(2).plusHours(12).plusHours(restHours))
                .endDate(startDate.plusDays(2).plusHours(12).plusHours(restHours).plusHours(minHours))
                .build();

        shiftService.createShift(shift1);
        shiftService.createShift(shift2);
        shiftService.createShift(shift3);
        shiftService.createShift(shift4);

        assertEquals(4, shiftRepository.findAll().size());
    }
}
