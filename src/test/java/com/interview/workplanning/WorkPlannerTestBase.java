package com.interview.workplanning;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Import(WorkPlannerTestConfiguration.class)
public class WorkPlannerTestBase {

    @Value("${shifts.validations.min-hours}")
    protected Integer minHours;
    @Value("${shifts.validations.max-hours}")
    protected Integer maxHours;
    @Value("${shifts.validations.rest-hours}")
    protected Integer restHours;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected RabbitTemplate rabbitTemplate;
}
