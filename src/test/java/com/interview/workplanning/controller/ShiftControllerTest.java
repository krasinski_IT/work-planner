package com.interview.workplanning.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.workplanning.WorkPlannerTestBase;
import com.interview.workplanning.domain.Shift;
import com.interview.workplanning.model.ShiftDto;
import com.interview.workplanning.model.ShiftPatchDto;
import com.interview.workplanning.repository.ShiftRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ShiftControllerTest extends WorkPlannerTestBase {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ShiftRepository shiftRepository;

    private static final String BASE_URL = "/v1/api/shifts";

    @BeforeEach
    public void setUp() {
        shiftRepository.deleteAll();
    }

    @Test
    void shouldPostShift() throws Exception {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 6, 0, 0);

        ShiftDto shiftDto = ShiftDto.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours))
                .build();

        this.mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(shiftDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.workerId", equalTo(workerId)))
                .andExpect(jsonPath("$.startDate", equalTo("2021-03-25T06:00:00")))
                .andExpect(jsonPath("$.endDate", equalTo("2021-03-25T14:00:00")));

        assertEquals(1, shiftRepository.findAll().size());
    }

    @Test
    void shouldReturnBadRequestWhenShiftIsNotValid() throws Exception {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 6, 0, 0);
        LocalDateTime endDate = startDate.plusHours(restHours).minusSeconds(1);

        ShiftDto shiftDto = ShiftDto.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        this.mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(shiftDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        assertEquals(0, shiftRepository.findAll().size());
    }

    @Test
    void shouldReturnBadRequest() throws Exception {

        ShiftDto shiftDto = ShiftDto.builder().build();

        this.mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(shiftDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        assertEquals(0, shiftRepository.findAll().size());
    }

    @Test
    void getShiftsForWorkerIdAndDates() throws Exception {

        String workerId = "worker-1";

        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 14, 0, 0);
        Shift shift1 = Shift.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(LocalDateTime.of(2021, 3, 25, 22, 0, 0))
                .build();

        LocalDateTime endDate = LocalDateTime.of(2021, 3, 26, 22, 0, 0);
        Shift shift2 = Shift.builder()
                .workerId(workerId)
                .startDate(LocalDateTime.of(2021, 3, 26, 14, 0, 0))
                .endDate(endDate)
                .build();

        shiftRepository.save(shift1);
        shiftRepository.save(shift2);

        assertEquals(2, shiftRepository.findAll().size());

        this.mockMvc.perform(get(BASE_URL + "/{workerId}", workerId)
                .param("startDate", startDate.toString())
                .param("endDate", endDate.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteShift() throws Exception {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 6, 0, 0);

        ShiftDto shiftDto = ShiftDto.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours))
                .build();

        this.mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(shiftDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        assertEquals(1, shiftRepository.findAll().size());

        Shift shift = shiftRepository.findAll().get(0);

        this.mockMvc.perform(delete(BASE_URL + "/{id}", shift.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        assertEquals(0, shiftRepository.findAll().size());
    }

    @Test
    void shouldPatchShift() throws Exception {

        String workerId = "worker-id-1";
        LocalDateTime startDate = LocalDateTime.of(2021, 3, 25, 6, 0, 0);

        ShiftDto shiftDto = ShiftDto.builder()
                .workerId(workerId)
                .startDate(startDate)
                .endDate(startDate.plusHours(maxHours))
                .build();

        this.mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(shiftDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        assertEquals(1, shiftRepository.findAll().size());

        Shift shift = shiftRepository.findAll().get(0);

        ShiftPatchDto shiftPatchDto = ShiftPatchDto.builder()
                .startDate(startDate.plusDays(1))
                .endDate(startDate.plusHours(maxHours).plusDays(1))
                .build();

        this.mockMvc.perform(patch(BASE_URL + "/{id}", shift.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(shiftPatchDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        assertEquals(1, shiftRepository.findAll().size());

        Shift updatedShift = shiftRepository.findAll().get(0);

        assertEquals(shiftPatchDto.getStartDate(), updatedShift.getStartDate());
        assertEquals(shiftPatchDto.getEndDate(), updatedShift.getEndDate());
    }
}
