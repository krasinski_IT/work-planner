# Work planning app

- [Swagger](http://localhost:8080/swagger-ui.html)

## Requirements

- JDK 11
- Maven 3

## Launching the app with mocked RabbitMQ

```
mvn spring-boot:run
```

## Launching the app with RabbitMQ on docker

```
docker run -d -p 5672:5672 -p 15672:15672 --name my-rabbit rabbitmq:3-management
```

```
mvn spring-boot:run -Dspring-boot.run.profiles=rabbit-docker
```

## Running test

```
mvn test
```
